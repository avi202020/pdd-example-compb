package cliffberg.pddexample.compb;

public class ID {
	String id;
	/* Verify that the ID matches the syntax required for all IDs. */
	public ID(String id) throws Exception {
		validate(id);
		this.id = id;
	}
	private void validate(String id) throws Exception {
		if (id == null) throw new Exception("null string");
		if (id.length() == 0) throw new Exception("Empty string");
	}

	public boolean equals(ID otherId) {
		return this.id.equals(otherId.id);
	}
}
